const express = require('express')
var bodyParser = require('body-parser');
const app = express()
const port = 3000

/*This will infrom nodejs server about web directory
This will load index.html by default.
*/
app.use(express.static(__dirname ))
app.use(bodyParser.json())

//Configure Autodriver routes.
require('./server/autodriver/autodriver.config.js')(app);



app.listen(port, () => console.log(`Example app listening on port ${port}!`))