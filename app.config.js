﻿'use strict';

    angular
        .module('covid19')
        .config(['$routeProvider',
        function config($routeProvider) {
                $routeProvider
                    .when('/', {
                        controller: 'RegisterController',
                        templateUrl: 'register/register.view.html',
                        controllerAs: 'vm'
                    })

                    .when('/donor', {
                        controller: 'DonorController',
                        templateUrl: 'donor/donor.view.html',
                        controllerAs: 'vm'
                    })

                    .when('/register', {
                        controller: 'RegisterController',
                        templateUrl: 'register/register.view.html',
                        controllerAs: 'vm'
                    })

                    .when('/home', {
                        controller: 'HomeController',
                        templateUrl: 'home/home.view.html',
                        controllerAs: 'vm'
                    })

                .otherwise({ redirectTo: '/login' });
        }
        ]);