module.exports = function(app) {
    const autodriverCtrl = require('./autodriver.controller.js');

    app.post('/autodriver', autodriverCtrl.add);

    app.get('/autodriver', autodriverCtrl.getAll);
}