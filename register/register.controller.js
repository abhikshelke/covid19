﻿(function () {
    'use strict';

    angular
        .module('covid19')
        .controller('RegisterController', RegisterController);

    //RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    RegisterController.$inject = ['$http', 'FlashService'];
    //function RegisterController(UserService, $location, $rootScope, FlashService) {
    function RegisterController($http, FlashService) {
        var vm = this;

        var autoDriver = {
            autoNumber : '',
            name : '',
            mobileNumber : ''
        }
        vm.autoDriver = autoDriver;


        vm.register = register;

        function register() {
            var autoDriverList = [vm.autoDriver];
            $http({
                method : "POST",
                  url : "/autodriver",
                  data : JSON.stringify(autoDriverList)
              }).then(function (response) {
                if (response.data.success) {
                    FlashService.Success('Registration successful', true);
                
                } else {
                    FlashService.Error(response.message);
                }
            });
           /* vm.dataLoading = true;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Registration successful', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });*/
        }
    }

})();
